+++
title = "Opsamling på ølkalender 2014"
description = "Jeg nåede aldrig i mål med ølkalenderen 2014. Det vil sige jeg fik aldrig anmeldt alle øl."
template = "page.html"
date = 2015-01-03

[taxonomies]
categories = [ "beer" ]
tags = [ "beer" ]
+++

Jeg fik aldrig anmeldt alle øl i mine ølkalender for 2014, så her kommer der
lige en lille opsummering af min generelle oplevelse. Dem der havde set frem
til at få alle øl anmeldt må jeg hermed skuffe.

<!-- more -->

Overordnet synes jeg kalenderen var stykket godt sammen, der var øl for næsten
en hver smag. Du skal være en meget indskrænket øl person, hvis der ikke var en
eneste af de 24 øl du kunne lide.

Øllene kom fra belgien primært og så danske øl. Her bider jeg mærke i at der
vare flere øl fra Hancock bryggeriet og endda en julehvidtøl. Julehvidtøl er
ikke min kop øl, men jeg synes tanken var god, det høre sig til i en
øljulekalender.

Som sagt var jeg slet ikke bekendt med øllene fra Refsvindinge bryggeri og de
fleste øl derfra var faktisk gode. Det er sidste gang jeg går uden om dem, når
jeg skal handle øl.

Resten var belgiske øl og her er jeg kun beskendt med at fåtal af dem bl.a.
Gouden Carollus og Chimay. Alle de andre var ganske gode øl svingende fra 6%
til 12% i styrke. Nogle var kun hygge øl og andre var faktisk ret gode til den
fede julemad. Den jeg nok bedst kunne lide var Brugse Bok fra Den halve maan.

Jeg synes bestemt at kalenderen var godt stykket sammen og det personlige præg
med egen indpakning og leveringen trækker bare endnu mere op og vil anbefale
alle der synes om øl at bestille den og jeg skal da prøve den igen, dog ved jeg
ikke om det bliver i 2015.
