+++
title = "Mars sucks"
description = "Why go to Mars when se already have mother Earth"
date = 2021-09-25

[taxonomies]
categories = ["quote"]
tags = ["satire", "climate change"]

[extra]
image = "mars-sucks-earth.webp"

+++


> Mars sucks. Its weather sucks. Its distance sucks. Its atmosphere sucks. The
little water it has sucks. It has sucked for billions of years and will suck
for billions more. You know what doesn’t suck? Me. Earth. I have life. I have
vast oceans and lush forests. I have rivers to swim and air to breathe. But the
way I’m being treated, that part sucks. You use me and pollute me. You overheat
me. You use every resource I have and return very little back from where it
came. > And then you dream of Mars. A hellhole. A barren, desolate, wasteland
you can’t set foot on fast enough. Why not use some of that creative energy and
billions on saving me? You know, the planet that’s giving you what you need to
live right now. Mars can wait. I can’t.

&mdash; Earth

<!-- more -->

[Mars Sucks](https://www.marssucks.com/)
