+++
title = "Choosing a twitter client"
description = "It is not alway easy to choose the right twitter client, but Federico Viticci from macstories.net tries to cast light on the matter"
template = "page.html"
date = 2014-12-13

[taxonomies]
categories = [ "social" ]
tags = ["goodread", "thirdparty", "longread", "ios"]

[extra]
image = "images/Twitter_logo_blue.webp"
+++

It is not alway easy to choose the right twitter client, but [Federico
Viticci](http://www.macstories.net/author/viticci/) from
[macstories.net](http://www.macstories.net) tries to cast light on the matter
in his segment called "[Twitter Clients in 2014 = An Exploration of Tweetbot,
Twitterrific, and Twitter for
iOS](http://www.macstories.net/stories/twitter-clients-in-2014/)"
<!-- more -->

I do agree that the official twitter client gives the best experience in most
cases and I end up using two clients. [Tapbot's
Tweetbot](http://tapbots.com/software/tweetbot/) for my lists and the official
twitter app for the rest. As of now Tweetbot is now my feedreader wih easy
access to extension when I can save to [pocket](http://getpocket.com/) for
later or [buffer](http://bufferapp.com/) a retweet because twitter does not
take advantage of the new share sheets in iOS8.
