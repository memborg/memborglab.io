+++

title = "Mere bevægelse "
description = "Jeg er begyndt at røre mig mere med futsal og tage trappen"

date = 2022-02-03

[taxonomies]
categories = ["health"]
tags = ["futsal","biking"]

+++

Jeg er begyndt at ville bevæge mig mere end jeg normalt gør. Jeg har i mange år cyklet til og fra arbejde. Typisk har der været ca. 5km hver vej og det tager omkring 20 minutter at cykle turen. Pulsen kommer lidt op, men kun hvis jeg vil det. 

<!-- more -->

## Fodbold og Futsal

I august blev jeg spurgt om jeg ikke ville deltage i noget hygge udendørs fodbold for mænd på omkring min egen alder. 

Jeg har ikke spillet fodbold i næsten 20 år og kunne derfor ikke sige nej til tilbuddet om starte og se om jeg stadig havde det i mig. Overraskende nok var min spilforståelse ikke forsvundet, men det var min kondition til gengæld. Hverdag på cyklen havde ingen effekt på udholdenhed, når der skal soilles fodbold. Det var super hårdt og alle led og muskler i benene var ømme i en hel uge. Specielt mine knæ var helt ødelagt. Teknikken er også meget rusten, så boldkontrollen er ikke så god og afleveringerne lå heller ikke der hvor det var planen.

Det fede er at vi ved godt vi ikke er 25 længere og derfor er der fokus på at vi varmer op og npr spillet køre går vi til den med måde. Der er ingen grund til at få en skade med hjem, hvis den kunne vlre undgået.

Der er også noget socialt i det, som jeg ikke får på arbejde eller der hjemme med familien.

Da udendørs sæsonen sluttede var der mulighed for at være med på klubbens [futsal](https://da.m.wikipedia.org/wiki/Futsal) hold på de samme betingelser som udendørs. 

Jeg har aldrig spillet futsal før, men kun den klassiske indendørs fodbold med bander. Jeg kendte ikke reglerne på forhånd og jeg er stadig ikke helt inde i hvordan man bevæger sig rundt på banen. 

* Hvor stiller man sig forsvaret?
* Hvordan bevæger man sig rundt i angrebet?
* Hvilke tatikker kan man benytte bed dødbolde?

Det er et simpelt spil. 5 mand på banen, hvor én agere målmand og må tage bolden med hænderne. Vi spiller med flydende udskiftning, men der er vist også regler for det. Ellers er det bare fuld knald på i 10-15 minutter.

Jeg kan dog mærke min boldføling bliver bedre og bedre.

## Mere bevægelse

Når nu min kondition er så lav er jeg begyndt at tænke på at løbe. Hvor kunne det være fedt at kunne løbe 5km uden pause. Somsagt er jeg stadig i overvejelses fasen.

En anden ting jeg allerede kan gøre, er at tage trappen op til lejligheden. Jeg bor på syvende sal og jeg starter på minus et. Det giver ni etager jeg skal op for at komme hjem. Mit første mål er at jeg kan gå op af alle de trin uden at blive forpustet. 

Lad os se hvordan det går og held og lykke til mig.
