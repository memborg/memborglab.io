+++
title = "Coffee Dripper"
description = "I received the Clever Dripper as a present for my birthday and it is the easiest way to brew a simple cup of filter coffee. No fuzz, just coffee"
date = 2024-06-14

[taxonomies]
categories = ["hobby"]
tags = ["coffee"]

[extra]
image = "clever-dripper.webp"
+++

I received the [Clever
Dripper](https://eightouncecoffee.ca/products/clever-dripper) as a present for
my birthday and it is the easiest way to brew a simple cup of filter coffee. No
fuzz, just coffee.
<!-- more -->

I'm not being sponsored I just really like it and the simplicity. No need for
a machine on your kitchen counter. All you need is a way of boiling water,
grounded coffee beans, a large cup, and the dripper. If you want to be a bit
more precise about how you drip your coffee a kitchen weight or even better
a precise coffee weight would also be handy.

This setup makes sense to me as I'm the only coffee drinker in the household.
If we were more people drinking coffee a regular coffee machine would be
needed.

From the time you have added the coffee till it is done it takes about two and
a half minutes for a large cup of coffee. So it is rather quick too compared to
a regular coffee machine.

## The recipe:

Dose: 18.5g | Water weight: 310g | Ratio: 16.75 : 1 | Brew time: 2:30

### Prep protocol

* Fill kettle to maximum volume, and set to boil (100°C / 212°F)
* Carefully fold seams of paper filter to allow filter to sit flush in the dripper
* Rinse the filter with hot water to remove paper taste and also to heat the brewer
* Empty water from dripper, place on scale, tare

### Brewing protocol

* Add boiling water 100°C to full weight (310g)
* Add coffee grinds to the water, start timer
* Use spoon to break-up all dry clumps while stirring the grinds to ensure
  a full saturation
* From 0:25 at the latest, let it steep until 1:10
* At 1:10 break crust by stirring with spoon, pulling residual grounds off of
  the filter walls, and stir two full rotations
* Place dripper on top of carafe/cup to activate drain-valve while stirring
  4 more times to initiate a vortex and wait for coffee to fully drain
* At 2:30 check water level (all brewing water should be drained, but it may go
  as long as 2:45)
* Enjoy!
