+++
title = "Hvad tænker en udvikler på?"
description = "Den anden dag fik jeg spørgsmålet \"Hvad tænker en udvikler på når han skal svare på et spørgsmål?\""
template = "page.html"
date = 2018-01-03

[taxonomies]
categories = [ "programming" ]
+++

Lad os aflive myten med at der glider strømme af nuller og ettaller ned over
hans nethinde. Der bliver heller ikke tænkt i kildekode og typisk heller ikke
matematiske formler.

<!-- more -->

![Niks, det sker ikke.](/images/hackers_spacesymbols.gif)

Typisk tænkes der i flows. F.eks. hvad sker der når brugeren trykker på den
grønne knap eller hvad sker når vi forsøger at sende til en e-mail der ikke
findes.

Det er mere evnen til at tænkes logisk og foretage abstraktioner, der senere
kan omdannes til kildekode og noget computeren i sidste ende kan forstå.

Denne evne er opstået ved, at man, som udviklet har kodet forskellige systemer
og små programmer i lang tid. For mange udviklere lige siden de fik deres
første computer og opdagede at den ikke kun skulle bruges til at spille spil,
men kunne også bruges til at skabe spil eller programmer.

For mit vedkommende skete det i en ret sen alder, at jeg opdagede at en
computer kunne bruger til andet end at spille på. Min første dims var en NES og
senere en GameBoy. Jeg vik først en pc, som teenager og brugte den til at
spille på indtil jeg fik behov for at lave små scripts der kunne convertere WAV
filer om til MP3, så jeg slap for at skrive den samme kommando igen og igen. Og
samtidig fik jeg lyst til at prøve andet en Windows og bestilte min første
RedHat distro cd og forsøgte at få den op at køre. Siden hen tog jeg en
datamatiker og forsøgte mig også på AAU med at tage en bachelor i datalogi, men
døde på den abstrakte matematik og dens beviser. Så NP vs P og turingmaskiner
er ikke lige mig og jeg kommer nok heller ikke til at skrive et bevis for at
min kompiler er korrekt.

Nå, tilbage til det egentlige spørgsmål og det er ret simpelt, En udvikler
tænker ikke i kode eller formler men mere i processer der kan hjælpe brugeren
med, at løse hans opgave bedst mulig.

Og jeg kan lige slutte af med denne.

![This is why you shouldn't interrupt
a programmer](/images/oYpue.webp)
