+++
title = "Mind dig selv om disse"
description = "Giv slip på fortiden - Du er ny person hverdag. Hverdag er en ny start og glem hvad du gjorde og sagde i går. Hvad der betyder noget er at du har gode intentioner og du gør dit bedste hver dag."

date = "2019-03-16"

[taxonomies]
categories = ["wellbeing"]

+++

## Giv slip på fortiden 

Du er ny person hverdag. Hverdag er en ny start og glem hvad du gjorde og sagde
i går. Hvad der betyder noget er at du har gode intentioner og du gør dit
bedste hver dag. 

## Skynd dig langsomt 

Vi har altid travlt og vores liv, men det netop det vi skal leve og ikke skynde
os med livet. 

<!-- more -->
 

## Skab din egen fremtid 

Bare fordi du skynder dig langt og i lever i nuet, betyder det ikke du skal
glemme i morgen. Faktisk skal vi kan vi skabe vores egen fremtid ved at tage
bevidste beslutninger. 

Hvem er du? Hvad det for et liv du vil leve? Brug dine beslutninger til at
skabe dit liv. Tilpas dit liv til den du er. 

## Du har et godt liv 

Det er nemt at komme til at sammenligne dit liv med andres, men du kan ikke
sammenligne lykke, familie, helbred. Dit liv er godt, som det er og det er af
den simple grund at du kun har ét liv. Men det betyder ikke du ikke kan ændre
noget. Noget der er godt kan stadig bliver bedre. 

## Motion 

Du skal have motion hverdag. Der er ikke noget der hedder at jeg springer lige
over i dag. NEJ, det kan du ikke. 

Du skal røre dig hver dig ellers ender det med at forfalde eller blive restløs.
Bare 30 minutter er nok. 

## Læs de bøger du finder spændende 

Læs kun de bøger du ikke kan give slip på og de bøger du finder kedelige skal
du bare ligge til side. 

## Du kan ikke nå det hele på én dag 

Du kommer aldrig til nå alt det du har sat dig for på én dag. Tag en beslutning
og hold dig til den plan og spred det ud over hele ugen. 

## Før dagbog 

At føre dagbog hjælper dig med forbedre dit logiske sans, talegaver og
generelle tanke gang. Det hjælper dig også med at falde til ro og reflektere
over det du gør. 

## Alle har problemer 

Næste gang du forventer noget fra dit mennesker du holder af prøv så at
forestiller dig i deres sko. Du er ikke den eneste der har problemer og et
hårdt liv. Vi alle i samme båd og ved at være bevidst om dette gør det nemmere
at for menneskerne i vores liv. 

## Du TAGER tid 

Det er nemt at komme den til den konklusion har du ikke har tid til de ting du
burde gøre og det gælder alle. 

Forskellen er at det er en bevidst beslutning at TAGE sig til det der er
vigtigt. 

## Slap dog af! 

Det er nemt, at komme til at tage livet alt for seriøst, specielt når du nu
arbejder så hårdt for at forbedre det. 

Men vi er i gang med at finde ud af livet og ingen har svaret, selvom der er
nogen der tror de har. Så slap dog af, det er bare et liv. 

## Arbejd for det - alt falder fra hinanden 

Alt falder fra hinanden her i livet . Det er en naturlig del af det.  

Men hvis du ikke selv yder en indsats for de mennesker du holder af, falder det
hele fra hinanden af sig selv. 

## Hold op med at være en kylling 

Find dig ikke i noget. Respekter dig selv og hav en mening. Gør noget. 

Duft til livet 
