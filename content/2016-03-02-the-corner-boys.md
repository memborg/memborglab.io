+++
title = "The Corner Boys"
description = "Two dealers of illegal substances standing on a corner trying to make a sale"
template = "page.html"
date = 2016-03-02

[taxonomies]
categories = [ "video" ]
tags = ["funny", "vimeo", "compilation"]

[extra]
image = "images/450188084.webp"
+++

Two dealers of illegal substances standing on a corner trying to make a sale
and trying to kill time while waiting.
<!-- more -->

## Corner Boys 1
<iframe src="https://player.vimeo.com/video/75576061?title=0&byline=0&portrait=0" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/75576061">Corner Boys</a> from <a href="https://vimeo.com/grainmedia">Grain Media</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

## Corner Boys 2
<iframe src="https://player.vimeo.com/video/87857384" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/87857384">Corner Boys #2</a> from <a href="https://vimeo.com/grainmedia">Grain Media</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

## Corner Boys 3
<iframe src="https://player.vimeo.com/video/120710972?color=E24A8D&byline=0&portrait=0" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/120710972">Corner Boys #3</a> from <a href="https://vimeo.com/grainmedia">Grain Media</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

## Corner Boys 4
<iframe src="https://player.vimeo.com/video/156825106?color=E24A8D&byline=0&portrait=0" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/156825106">The Corner Boys #4</a> from <a href="https://vimeo.com/grainmedia">Grain Media</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
