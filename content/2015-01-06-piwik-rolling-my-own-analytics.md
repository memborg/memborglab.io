+++
title = "PIWIK - rolling my own analytics tool"
description = "PIWIK is my opertunity  to roll my own analytics tools and stop relying on the mighty Google Analytics."
template = "page.html"
date = 2015-01-06

[taxonomies]
categories = [ "writing" ]
tags = ["analytics", "piwik"]

[extra]
image = "images/piwik_logo.webp"
+++

PIWIK is my opportunity to roll my own analytics tools and
stop relying on the mighty Google Analytics. The tool is based on PHP and MySQL
as every other free webservice almost is. And as every web hosting today in
Denmark is serving PHP and MySQL, then why not.
<!-- more -->

PIWIK is pretty and have some nice features, besides that it notifies me when
there's an update and it respects the Do-Not-Track request header. Ican can
even host several other sites and give acces to other people if I want to.
This is another step to be independent of Google.

![The
dashboard](/images/piwik-dashboard.webp "A complete view of the dashboard")

So far I'm liking it.

![The
dashboard](/images/piwik-mobile-dash.webp)
