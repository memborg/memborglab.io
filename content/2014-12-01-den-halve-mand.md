+++
title = "Halvmånen i Brügge"
description = "Min første øl i min ølkalender er fra halvmånen i Brügge"
template = "page.html"
date = 2014-12-01

[taxonomies]
categories = ["beer"]

[extra]
image = "images/dehalvemaan.webp"
+++

Årets første øl i min ølkalender er en **Brugse Bok** fra [De halve
maan](http://www.halvemaan.be/en/home) i Brügge Belgien. Det er ikke en øl jeg
er bekendt med, dog har jeg efterhånden drukket en del belgiske øl og jeg ved
hvad jeg kan forvente.
<!-- more -->

Og jeg blev ikke skuffet og jeg synes det er en god start på kalenderen. Øllen
den er mørk, dejlig rund og tilpas stærk, 6.5% alkohol er meget passende. Den
gør det fint, som mad øl og passede godt til flæskesteg med hele svineriet til
her faldt den på et tørt sted.

Så er vi i gang.

![De halve maan - Brugse Bok](/images/brugsebok.webp)
