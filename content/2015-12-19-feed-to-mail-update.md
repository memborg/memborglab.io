+++
title = "Feed to mail [UPDATED]"
description = "I was getting tired of Ifttt.com and wanted more control of how the feed got delivered into my mailbox."
template = "page.html"
date = 2015-12-19

[taxonomies]
categories = [ "programming" ]
tags = ["code", "developer", "python"]
+++

It's been a month since I [posted](@/2015-11-01-feed-to-mail.md) the
initial version of feed2mail.python and now I have some updates for you.
<!-- more -->

## Updates

### Aggregation

It is now possible to aggregate your feeds by the hour. And get the feed sent
to you several hours every day. Say you have a news feed which is very active
and you don't want an email every time there's a new post. You can now define
on which hours you want en aggregated email ex. at hour 07, 12 and 17.

The aggregated email contains an index with a link to anchor on the entry. You
should think that a named anchor would work in all email clients, but No! The
email client which supports the most CSS and markup, the Apple Mail, does not
support any form of anchors and I cannot get my head around it. Why would they
not support anchors? But guess who supports anchors? Yeah, that's right Outlook
and surprisingly Gmail does too, but on named anchors. Id anchors are a no go
for Gmail.

Email client have once again proved that "rich" emails are a pain in the ass,
even simple ones.

### Entry handling

Several feeds contains an enclosure tag, which is a list of attached media.
Images, document, video and so on. Now the enclosures' comes with a link to the
media and a mime type. This parsed and changed to a corresponding HTML tag. Ex.
images are converted to a `IMG` tag. Documents are just wrapped in a `A` tag
and trying to do fancy stuff videos are wrapped in a `VIDEO` tag. I though this
would be a good idea. An email with en embedded video, at least this should
work in the Apple Mail, but again I'm disappointed again. The client shows
a nice placeholder and a play button, but nothing happens.

### Small change

* There's a config.ini for all your configuration needs.
* Buffer gets trimmed
* Created a launch agent for OSX
* Ignore entries with dates older than current date

## SMTP challenges

This has been the most challenging part. To get the mails sent, from my own
server. I have dynamic IP from my ISP and I want it to stay that way, no need
for a static IP. I started using the SMTP server from my web hotel. This ended
quickly they locked the email account until I changed my password. So this was
a no go because this would happen fairly often.

Then I tried my ISP SMTP server. That went okay for a couple of weeks. There
were dropouts in the mail delivery, but it worked. Then suddenly mails stopped
flowing with no warning. Changing password on the mail account I was using
didn't help. Now I'm trying to send through my outlook.com account to see how
long that will last.
