+++
title = "Trappisten fra Westmalle"
description = "Endnu en gang skal vi til Belgien og det nok ikke sidste gang. Denne gang er det en dubbel munke øl fra Westmalle"
template = "page.html"
date = 2014-12-03

[taxonomies]
categories = [ "beer" ]
tags = ["belgian", "beer", "3star"]

[extra]
image = "images/Westmalle_Dubbel_Logo-500x500.webp"
+++

Ja, jeg er lidt uinspireret. Det er ikke toppen af poppen, men mere en jævn øl.
<!-- more -->

Jeg har fået bedre trappist øl, så vi stopper bare her. Det bedste er den fine
indpakning.
![Westmall](/images/westmalle.webp)
