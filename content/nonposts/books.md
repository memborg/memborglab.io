+++
template = "books.html"
path = "books"

title = "Books"
description = "A list of all the books I have read over time"
+++

This is a list of all the books I have read over time. The list is
extracted from [Calibre](https://calibre-ebook.com/).

The list is sorted by rating and then title and best rated books are at the
top. The rating is my own
