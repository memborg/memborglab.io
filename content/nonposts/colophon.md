+++
# Template to use to render this page
template = "nonpost.html"
path = "colophon"

title = "Colophon - or what this site is built with"

description = "This site i build with"

+++
> A colophon is a page or section, like a footer, of a site that describes how the site is made, with what tools, supporting what technologies.
>
> [Colophon - IndieWeb](https://indieweb.org/colophon)

## Techonologies

* Site build with [Zola](https://www.getzola.org/).
* CSS based on the [missing.style](https://missing.style/) styling.
* Hosted on [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).
* DNS handled at [Hetzner](https://www.hetzner.com/dns-console/).
* Domain handled at [.dk](https://punktum.dk/).
* I provide an [RSS](/rss.xml) or [atom](/atom.xml) feed so you can subscribe
  to my blog.

### A small thing

This page mostly lives up to what is described in [Web page annoyances that I don't inflict on you here](https://rachelbythebay.com/w/2025/01/04/cruft/). Though this part:

> I don't set cookies. I also don't send unique values for things like
> `Last-Modified` or `ETag` which also could be used to identify individuals. You
> can compare the values you get with others and confirm they are the same.
>
> I don't use visitor IP addresses outside of a context of filtering abuse.

I am unsure of, as I am hosted at GitLab and they do send `ETags` and
`Last-Modified` headers. What GitLab does with your IP address I do not know
either.

The last part I haven't tried is how the page performs through Telnet. If you
do try it, let me [know how](/hello) is was.
