+++
template = "nonpost.html"
path = "membot"

title = "This is my MemBot"
description = "This is my useragent for my MemBot projects ad scrapers"
+++

This is my useragent when doing something automatic involving doing HTTP
request to your website.

Do you have any questions, please contact my on <membot@superrune.dk>
