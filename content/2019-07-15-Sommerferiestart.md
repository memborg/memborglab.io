+++
title = "Sommerferiestart"
description = "Min sommerferie er lige startet og det er tid til at reflekterer og samle viden."
template = "page.html"
date = 2019-07-15

[taxonomies]
categories = [ "planning" ]
+++

Jeg bruger altid noget af min ferie på at lytte til podcasts og denne ferie er
ikke anderledes. Det er typisk arbejdsrelateret podcasts omkring Scrum, Kanban
og Agile, i kraft af jeg selv er produkejer og hele min organisation er gået
Scrum.
<!-- more -->

Den første podcast jeg har lyttet til er [Professional Scrum with Kanban – Yuval Yeret](https://ryanripley.com/afh-108-professional-scrum-with-kanban-yuval-yeret/)
med Ryan Ripley. Her taler de en del om flow og hvordan holder vi flow i vores
sprint. Helt lavpratisk var der en scrummaster der have klistret små stykker
bananskræl på hver opgave for følge med i hvordan opgaven langsomt rådnede. Jo
længere tid en opgave hang på tavlen med skrællen på desto mørkere blev
skrællen og i sidste ende begyndte den at lugte. Opgaven var i bogstaveligste
forstand rådnet, fordi ingen have brugt tid på den.

Det viser bare at der er mange måde at håndtere flow og skabe flow i et sprint
eller bare helt overordnet. Det er lige meget om det er i et udviklignsteam
eller et support team.

Må I alle have en god sommerferie.

Måske kommer der mere i løbet sommeren ellers sker nok noget efter min ferie er
endt.
