+++
title = "Did they say No?"
description = "It is hard to say no, and what is the underlying reason not saying no"
date = "2023-10-30"

[taxonomies]
categories = ["management"]
tags = ["leadership", "people", "communication", "collaboration"]

[extra]
image = "no_.webp"
+++

It is hard to say no, and what is the underlying reason not saying no.

One of my favourite Substack’s "The Beautiful Mess" had a post with a [list of reasons](https://cutlefish.substack.com/p/tbm-246-why-didnt-they-say-no) on why the team didn’t say No. 

I like most of them but this one below is the one that struck me most 

> 7. Saying no takes confidence. It can be hard to draw the line if your confidence has been beaten down or never lifted up. 

Which one is you favourite reason for people not saying No! 

<!-- more -->
