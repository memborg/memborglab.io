+++
title = "Hacky Cooling Solution"
description ="Sometimes you come up with a hacky solution to a hardware problem. That is what I did today."

date = "2023-02-25"

[taxonomies]
categories = ["programming"]
tags = ["hack","network"]
+++

Sometimes you come up with a hacky solution to a hardware problem. That is what I did today. 
<!-- more -->
I have an old Intel NUC which is running [Pi-hole](https://pi-hole.net/) and many other small local network utilities including a DHCP server. 
Every night it shuts down and powers on again early the next morning. It is so old now that the battery powering the BIOS is out of juice and it cannot be replaced anymore because it is not a standard battery. This is not a problem.
The problem is that the cooling fan fails to turn on at first boot 100% of the time. When it fails to turn the computer runs hot and powers down when getting too hot. One way to mitigate this is to reboot the machine and the cooling fan starts to spin. 
To do an automatic reboot I created a small application that monitors the CPU temperature and if it gets too high it reboots the PC in hope of starting the cooling fan. If that fails ten times in a row then it shuts down to protect itself. 
It is not the best solution but it will keep the system going until I find a suitable replacement. 

The small project is found at [monitor_temp](https://gitlab.com/memborg/monitor_temp)
