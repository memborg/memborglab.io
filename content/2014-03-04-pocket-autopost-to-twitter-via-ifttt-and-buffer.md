+++
title = "Pocket - Autopost to Twitter via IFTTT and Buffer"
description = "I'v been working on automating my autoposting of Pocket reads to Twitter with tags and finaly found a solution"
template = "page.html"
date = 2014-03-04

[taxonomies]
categories = ["programming"]

[extra]
image = "images/pocket-twitter-ifttt-buffer.webp"
+++

I have a pleasant setup for saving articles from [Twitter](http://twitter.com)
for later in Pocket, adding tags to articles. Reading the articles and marking
them as read in [Pocket](http://getpocket.com). When marked as read there's
a trigger in [IFTTT](http://ifttt.com) which adds the read article to
[Buffer](http://bufferapp.com) which then post my read article back to twitter
with my hashtags applied. I will not go into the basic of all the services,
that is for you to explorer.
<!-- more -->

## Saving for Later

In my twitter App, I've setup my readitlater client. And as mentioned earlier
it is Pocket. I've tried http://readability.com, but I didn't
like it. When you have connected to Pocket you just tap hold on the link you
would like save for later reading and a action menu pops up. ![Save for later
in
Twitter.app](/images/20140226_193652000_iOS.webp)

## In the Pocket

In Pocket you can add tags to every article you have saved. Be able to add
hashtags to your twitter post automatically prefix all you Pocket tags with
# and you have save yourself a lot of trouble and you can with no effort get
hashtags on your Pocket reads. ![Pocket later list with
articles](/images/20140226_193800000_iOS.webp)

## Getting Triggered

In IFTTT you must have Pocket, Twitter and Buffer authenticated before you can
continue. Are you done? Good. Now create a trigger for Pocket which tells IFTTT
to do with articles I have read in Pocket. Next is to you buffer as our action.
![IFTTT Pocket buffer
recipe](/images/20140226_193732000_iOS.webp)
In the text box under action you can fill out the text you want to post on
Twitter and beside Article title and a link to article you'll need to add
Pocket tags and as mentioned before we have already prefixed those with
# making them ready for twitter. One caveat is that the tags comes comma
separated and I haven't found any solution to this. This makes your tweets
a bit longer than necessay.

## Buffered Up

Now everyhting is setup for Buffer to auto post our read Pocket articles with
hashtags. If you are using the free account there is a limit on how many tweets
you can buffer. If your posting schedule is parse then you will notice that
some of you posts will not get buffered because you have reached the queue
limit. You can overcome this by increasing how often Buffer should post to
twitter to keep your queue under the limit. ![Buffer
queue](/images/20140226_193903000_iOS.webp)

## Closing Words

I hope this gives you an idea how to automate posting to Twitter with Pocket.
It is pretty simple when you know all the services and the biggest annoyance is
the commas between the hashtags when re-posting from pocket to twitter
otherwise I'm pretty happy with it. I'm quite positive this setup can be
achieved with another reader and a service similar to Buffer.
