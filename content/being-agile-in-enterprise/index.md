+++
title = "Being agile in enterprise"
description = "Dive into Google's venture into unexplored domains, blending startup agility with enterprise challenges, emphasizing the importance of an agile mindset for success in a new domain."
date = "2024-01-02"

[taxonomies]
categories = ["agile"]
tags = ["product management", "collaboration", "podcast"]

[extra]
image = "being-agile-in-enterprise.webp"
+++

This is an interesting podcast about how Google created a new product in an
entirely new domain and how they did in an enterprise environment.

<!-- more -->

Typically, these podcasts are about how start-ups are creating new products and
for teams in established companies it can be hard to relate to how start-ups
work contra how enterprise companies work.

* We touch how it is to work inside of Google and start something new.
* We learn about how Google is very supportive of new initiatives and how to
  promote them internally at Google.
* We learn about the product managers mindset in creating a new product is not
  much different in an enterprise setting compared to a start-up. A
* And most importantly we learn how important it is to be agile and not commit
  to roadmaps when you are trying to build something new in and entirely new
  domain.
* You must commit to the work you intended to do, but you also must communicate
  that we are experimenting with this new domain and thereby set the right
  expectations.

I really like this talk and it is nice to hear from someone working in and
enterprise setting and myself. So go listen to [The Bard blueprint | Creating
value, shipping fast, and advancing AI ethically | Jack Krawczyk
(Google)](https://review.firstround.com/podcast/episode-113)
