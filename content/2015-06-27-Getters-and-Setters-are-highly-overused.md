+++
title = "Getters and Setters are highly overused"
description = "Getters and Setters are highly overused and alot of other controlversial programming statements"
template = "page.html"
date = 2015-06-27

[taxonomies]
categories = [ "programming" ]
tags = ["programming", "bestpractice"]
+++

>I’ve seen millions of people claiming that public fields are evil, so they
make them private and provide getters and setters for all of them. I believe
this is almost identical to making the fields public, maybe a bit different if
you’re using threads (but generally is not the case) or if your accessors have
business/presentation logic (something ‘strange’ at least). I’m not in favor of
public fields, but against making a getter/setter (or Property) for everyone of
them, and then claiming that doing that is encapsulation or information hiding…
ha!

*by Pablo Fernandez*
<!-- more -->

I stumpled upon two pages with some controversial statements that I found to be
true, IMHO.

[20 controversial programming opinions](http://programmers.blogoverflow.com/2012/08/20-controversial-programming-opinions/) *by [Yannis Rizos](http://programmers.blogoverflow.com/author/yannisrizos/)*

[Hopefully More Controversial Programming Opinions](http://prog21.dadgum.com/149.html) *by James Hague*
