+++
title = "Programming sucks"
description = "So no, I'm not required to be able to lift objects weighing up to fifty pounds. I traded that for the opportunity to trim Satan's pubic hair while he dines out of my open skull so a few bits of the internet will continue to work for a few more days."
template = "page.html"
date = 2015-01-20

[taxonomies]
categories = [ "programming" ]
tags = ["labor", "programming"]
+++

> So no, I'm not required to be able to lift objects weighing up to fifty
> pounds. I traded that for the opportunity to trim Satan's pubic hair while he
> dines out of my open skull so a few bits of the internet will continue to
> work for a few more days. 

Read more [Programming Isn't Manual Labor, But It Still Sucks](http://mashable.com/2014/04/30/programming-sucks/)

<!-- more -->
