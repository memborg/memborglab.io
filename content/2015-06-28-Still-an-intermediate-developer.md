+++
title = "Still an Intermediate Developer"
description = "I'm an experienced developer, with lots of baggage. But reading this broad generalization I'm still an intermediate developer and I'm fine with that."
template = "page.html"
date = 2015-06-28

[taxonomies]
categories = [ "programming" ]
tags = ["skills", "statue", "code", "developer"]
+++

>A good intermediate developer needs less supervision. They can be trusted to
raise issues of code design, and play a valuable role in design discussions.
They are also the “workhorses” of the dev team. However, further mentoring and
higher level supervision is still vital.

*[by Matt Briggs](http://mattbriggs.net/blog/2015/06/01/the-role-of-a-senior-developer/)*

I'm an experienced developer, with lots of baggage. But reading this broad
generalization I'm still an intermediate developer and I'm fine with that.
There are still much to learn.
<!-- more -->
