+++
title = "New feed link"
template = "page.html"
date = 2015-11-01

[taxonomies]
categories = [ "site" ]
tags = [ "settings" ]
+++

Just a heads up. I'll be changing feed url to `/feed.xml` from
atom.xml soon. Github Pages are now supporting the jekyll-feed plugin. There
are both a `summary` and a `content` field in the new feed. Nothing changes
other than a more rigid feed generation.
