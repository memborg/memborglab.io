+++
title = "FUBARdemo"
description = "At The Meltdown 2002 an old friend and I created this demo and won"
date = 2021-04-25

[taxonomies]
categories = ["demoscene"]
tags = ["TMD", "nostalgia"]

[extra]
image = "images/fubardemo-header.webp"
+++

When I was young and still studying, an old friend and I attended The Meltdown
2002 demo and lan party at Skive Handelsskole. And the weeks up this event we
prepared this demo in his dorm room.

<!-- more -->

I remember having a lot of fun creating this demo and when we submitted the
demo we didn't know If we had any chance of winning. Some of the other groups
attending were very talented and showed some great techniques and had good
storytelling. I think it caught us by surprise that we won, but the crowd had
us as the chosen ones.

You can decide for yourself what you think of the demo, but now I think it's
goofy and should not be taken seriously at all. That might be the one thing
that won over the crowd.

This is the demo we created and I have converted from Flash to MP4 for your
convenience.

<video width="640" height="480" controls preload poster="/images/fubardemo-header.webp">
    <source src="/video/fubardemo.m4v" type="video/mp4"/>
</video>

At the time it was created with Macromedia Flash and I also think the models
was created in 3D Studio Max.

You can watch my [Elbo demo](@/2021-02-14-elbo-demo.md) I made on the spot at
TDM 2002.
