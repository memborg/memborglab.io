+++
title = "How to supercharge your engineering org"
description = "My notes from the podcast that I found useful. This podcast is filled with great nuggets about leadership"
date = "2023-08-21"

[taxonomies]
categories = ["podcast"]
tags = ["leadership", "teams", "communication", "collaboration"]

[extra]
image = "how-to-supercharge-your-engineering-org.webp"
+++

> ...half of everybody working as a software engineer joined the industry in the
last 10 years.

> So I'm gonna get more prescriptive and more detailed in my
communication. And that's actually counterproductive. 'cause that's actually
not what people need. people need the foundations and the strategy and the
system and the goal. they don't need sort of the, the work divided down into
smaller and smaller pieces.

<!-- more -->

> Jira's not built for building greatness. You can use it that way. But
fundamentally, it's about someone who's trying to create a sense of control and
a sense of predictability over it, while at the same time actually asking to be
surprised

> People should want to talk to each other, but shouldn't have to coordinate
with each other.

There are some good nuggets found in the podcast [How to supercharge your engineering org ](https://review.firstround.com/podcast/episode-102)
