+++
title = "New phone, maybe"
description = "This could be my new phone. A Microsoft Lumia 950xl"
template = "page.html"
date = 2015-11-01

[taxonomies]
categories = [ "wants" ]
tags = ["phone", "microsoft"]

[extra]
image = "images/Lumia_950_Marketing_03_DSIM.webp"
+++

Yes, I know! I'm add it again. There is a a new Windows Phone OS and a new
Microsoft Lumia. Since the last price bump for an iPhone 6s I'm having a hard
time justifying the price tag. When I look at the Lumia 950xl and the pricing
and features it look more what I want from a phone.
<!-- more -->

I am almost certain that I'll never buy a Android phone. First off it is
infected with Google secondly the best Android phone comes from Samsung. An
phone and an OS from some of the worst companies in the world, IMHO, is a no
go.

I know that Microsoft have a different plan for mobile than Google and Apple
and it seems to be down playing the mobile version of Windows, but I want
Microsoft to have success with Windows Phone and the 10th verison looks
promissing while it may not as shiny as the others.

[Well, go decide for yourself here](https://blogs.windows.com/devices/2015/10/14/in-photos-the-microsoft-lumia-950-lumia-950-xl-and-lumia-550/)
