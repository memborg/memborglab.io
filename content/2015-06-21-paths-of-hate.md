+++
title = "Paths of Hate"
template = "page.html"
date = 2015-06-21

[taxonomies]
categories = [ "animation" ]
tags = ["short", "war"]

[extra]
image = "images/Paths_of_Hate_02.webp"
+++

I came across this short film and I really enjoyed the visual style and
effects. It is about 10minutes long.
<!-- more -->

>"**Paths of Hate**" is a short tale about the demons that slumber deep in the
human soul and have the power to push people into the abyss of blind hate, fury
and rage.

![Still from short](/images/Paths_of_Hate_07.webp)

[You can see the short and read more about the director Damian Nenow here](https://vimeo.com/70633467)
