+++
title = "On micromanagement"
description = "Micromanagement is about trust and it goes both ways"
date = "2023-10-28"

[taxonomies]
categories = ["management"]
tags = ["leadership", "people", "communication", "collaboration"]

[extra]
image = "on-micromanagement.webp"
+++

I like this statement from [Micromanagement: The Unpopular Truth You Need to Hear](https://shamun.dev/posts/micromanagement)

> ...our collective aversion to micromanagement and how it might sometimes prevent
> us from effective management.

And it has me thinking about how the lack of trust and transparency is a symptom of micromanagement. 

<!-- more -->

If I don't trust you on the work you are doing, I am going to force micromanagement on you to be sure you are doing the work I want you to do. And this is a "me" problem as I need to let you do your job and not controlling the how the job is performed. If something goes haywire, then I should coach you in how to do better next time and not try to control the situation. 

If you are not transparent about the work, you job and try to hide then you will suffer from micromanagement. And this is a "you" problem. If you are transparent about the work, you do and your intentions, no matter your confidence level, you are giving everybody the chance to support you. And as a side effect this will also build trust in you because everyone knows what you are about to do. 
