+++
title = "#NoEstimates"
description = "I min søgen efter hvad en PO laver dukkede dette emne op."

date = "2018-10-18"

[taxonomies]
categories = ["work"]
tags = ["estimation", "planning"]

[extra]
image = "running-sushi.webp"
+++

I min søgen efter hvad en PO laver dukkede dette emne op.

Det er ikke meningen vi skal holde op med at estimere, men at vi overvejer om
den tid vi vil bruge på at estimere ikke var bedre på brugt på at kode.

<!-- more -->

Estimater skaber ingen værdi for nogen, så lad os holde det på et minimum.

Estimater er bare et gæt, men vil vi forlade os på et gæt? Uanset hvor
kvalificeret et gæt er det stadig et gæt.

Kan vi ikke bare bygge dimsen, måle på det og lære af det?

## Podcats

- <https://ryanripley.com/noestimates-with-vasco-duarte/>
- <https://ryanripley.com/afh-048-how-project-managers-can-fit-on-agile- teams-podcast/>
- <http://deliveritcast.com/ep58-no-estimates> 

## The #NoEstimates movement

[Ryan Ripley - The #NoEstimates Movement - #Path17](https://youtu.be/-eE7rtCQHI8)

Hvordan visualisere jeg hvordan det går?

<figure>
<img src="cumulative-flow-diagram.webp"
alt="Cumulative Flow Diagram" 
width="1025"
height="575"
/>
<figcaption>Cumulative Flow Diagram</figcaption>
</figure>
                             
[Et link omkring comulative flow diagrams](http://brodzinski.com/2013/07/cumulative-flow-diagram.html)

Hvordan griber jeg det an med at indføre #NoEstimates?

<figure>
<img src="improve-estimation.webp"
alt="Ways to improve estimation and minimisin your effort estimating" 
width="1024"
height="574"
/>
<figcaption>Ways to improve estimation and minimising your effort estimating</figcaption>
</figure>

<figure>
<img src="running-sushi.webp"
alt="Running sushi - En analogi til et flow baseret koncept med løbende målinger, prognoser og efterspørgsel." 
width="1024"
height="530"
/>
<figcaption>Running sushi - En analogi til et flow baseret koncept med løbende målinger, prognoser og efterspørgsel.</figcaption>
</figure>

[Deliver it: EP58 - No estimates](https://overcast.fm/+EPNmKAdKM)

Slicing, hvordan skærer vi en opgave så den skaber mest værdi for kunden.
Kræver øvelse. Dopler radar, giver et nu og her billede af hvordan vejret er.

<figure>
<img src="dopler-radar.webp"
alt="Analogi for nu og her opdateringe"
width="1024"
height="1021"
/>
<figcaption>Analogi for nu og her opdateringe</figcaption>
</figure>

Forecast, udsigt/prognose for den næste korte periode baseret på tidligere
data.



