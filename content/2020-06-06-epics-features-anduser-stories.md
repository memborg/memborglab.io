+++
title = "Epic vs features vs user stories"
description = "En anden måde at kigge på epics, features og user stories, som ikke er et hierarki."
date = 2020-06-06

[taxonomies]
categories = [ "agile" ]
tags = ["scrum", "value", "shared_understanding"]

[extra]
image = "images/Epic-Feature-Userstory-venn-header.webp"
+++

Når vi taler om Epics, Features og User Stories giver det for mig mere mening
at tale om dem som forskellige emner der sammen genererer value til vores
kunder.

<!-- more -->

Hvis vi taler om dem som et hierarki ender vi med at tale om løsningerne hele
vejen igennem og ikke så meget værdier. Det kommer til at ligge mellem
linjerne.

![Så ser de flest forholdet mellem dem i dag](/images/Epic-Feature-Userstory-hierarki.webp)

Her er en Epic bare fuzzy ide på en løsning. Epicen bliver så nedbrudt i mere
konkrete stumper kendt, som features og til sidst bliver featuren brudt ned den
mindste del User Stories. Og alle sammen tager udgangspunkt i en løsning på et
behov eller mål, som vi måske tror giver værdi til kunden, men vi har ikke
undersøgt det. Og det er ikke helt tydeligt, da vi kun er i løsningsmode og
derfor ikke får hele historien med.

## Epic

Det er her forretningen taler om mål og behov. F.eks. Når vi taler om at
fastholde licenser på [Produktet]. Det er ikke konkret. men det er vigtigt for
forretningen for at overleve. Her kunne man beskrive men man vil fastholde og
hvilke konsekvenser det har ikke at gøre noget og hvad man forventer der vil
ske hvis man gør noget.

## User story

Dette er de ønsker og behov en bruger har, som vi kan se opfylder behovet fra
forretningen om at fastholde licenser. Det kan f.eks. være medarbejderen ønsker
at få lettet arbejdsgange i [Produktet] så han ikke behøver at skifte til rundt
mellem flere konkurrende produkter for, at finde de oplysninger medarbejderen
søger.

## Feature

Dette er det teksniske aspekt af hvordan vi løser brugerens behov I user
storyen og behovet fra forretningen. Dette er det udvikleren og endda
projektlederen forholder sig til.

Se det hele som et Venn diagram hvor overlappet mellem alle tre områder giver
værdi til kunden.

![Hvor værdien er gemt](/images/Epic-Feature-Userstory-venn.webp)

Alt dette er ikke speceilt, det er bare en anden fortolkning af ord vi allerede
kender, som kan hjælpe med at skabe focus på value i stedet for løsninger.

Der er dog udfordringer i dag, med de værktøjer jeg kender er låst til en
hierarki og jeg derfor ikke selv kan bestemme hvordan jeg gerne vil arbejde med
epic, user stories og features.

Kilde:
[Epics, Features, and Stories, Oh My! w/ Scott Sehlhorst by LeadingAgile](https://soundcloud.com/leadingagile/epics-features-and-stories-oh-my-w-scott-sehlhorst)
