+++

title = "Reading list"
description = "Collection of things to read"
date = 2021-04-30

[taxonomies]
categories = ["programming"]
tags = ["reading"]

+++

Small collection of things to read

-   [You midget as well timestamp it](https://changelog.com/posts/you-might-as-well-timestamp-it)
-   Nullable Reference types in C# – Best practices
-   [From zero to hero: Optimizing Android app startup time](https://medium.com/skoumal-studio/from-zero-to-hero-optimizing-android-app-startup-time-eda9022c0963)
-   [Working with web content offline in SwiftUI apps](https://blog.artemnovichkov.com/swiftui-offline)
-   [Core Data and SwiftUI](https://davedelong.com/blog/2021/04/03/core-data-and-swiftui/)
