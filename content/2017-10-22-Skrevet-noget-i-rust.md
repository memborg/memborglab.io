+++
title = "Skrevet noget i Rust"
description = "For at lære noget nyt har jeg kigget på at skrive kode i Rust."
template = "page.html"
date = 2017-10-22

[taxonomies]
categories = [ "hobby" ]
tags = ["Programming", "Rust", "Learning"]
+++

Alle de værktøjer jeg har skrevet til mig selv, har altid været skrevet i C# og
det er der sådan set ikke noget galt i.
<!-- more -->

Jeg har forsøgt at tage alle mine Javascript skills og starte på at bruge
NodeJS, men til små værktøjer blev det bare for bøvlet. Jeg har endda vovet mig
ud i at kode Python 3 og det er nok der jeg har haft størst succes ud over C#,
dog virker det lidt af et monster at komme i gang med pakker og setuptools til
at jeg gider fortsætte eller starte et nyt projekt i Python.

En anden grund til at jeg har benyttet disse sprog er at de alle sammen er
cross-platform, altså virker på både Windows, \*nix eller Mac. Dette er dog
ikke aldrig sandheden. Ønsker man at bruge Mono på f.eks. Mac og skal lave
forspørgsler til et domæme med SSL er der stor chance for at det fejler fordi
der ikke er har været understøttelse af de senest TLS versioner eller lignende.
På Windows er der store udfordringer med at få de forskellige pakker fra NPM
eller PIP til overhovdet at kompile. Dette gælder også for f.eks. Ruby.

Fælles for disse sprog er at de alle har en eller anden form for virtuel
maskine nedenunder, som afvikler deres kode eller bytekode. Jeg har længe haft
lyst til at lave noget i C++ og udnytte noget af den rå kraft som et lowlevel
sprog kan levere, men hvor skal man starte og hvilken version af C++ skal man
starte med?

## Rust kommer ind

Jeg er startet med at kigge på Rust fordi deres til gang til et
programmeringsspring er anderledes end så mange andre nye sprog. De vil rigtig
gerne lave et sprog der har alle krafterne fra et lowlevel sprog og samtidig
have sikkerheden fra et managed sprog, som C# eller Java. Der er f.eks. ikke
nogen form for NULL værdier eller Exception håndtering(?!). Kompilerens statisk
analyse af koden og hvornår variabler er ude af scope og borrower konceptet,
skulle gerne resultere i mere regide og sikre programmer.

Et par små ting jeg er startet op på kan ses
[her](https://gitlab.com/memborg/guessing_game) og
[her](https://gitlab.com/memborg/pomodoro_timer). Det er ikke noge særligt, men
det er også mest for at lære hvordan Rust fungere og ikke fungere. Jeg har en
plan om at skrive min
[feed2html.python](https://gitlab.com/memborg/feed2html.python) om til Rust,
men så langt er jeg slet ikke endnu. Der er mange ting jeg ikke forstår endnu
og jeg kommer nok til at bruge en del tid på at læse ["The
Book"](https://doc.rust-lang.org/book) igennem og måske endda flere gange. Det
er en meget god bog med gode eksempler dog kræver det, at jeg sidder og koder
med, mens jeg læser bogen ellers for jeg det ikk eind under huden.

Jeg forsøger at holde mig til den stabile version af Rust og ikke begynde at
lege med nightlies. Dog er der nogle udfordringer der, hvis man ønsker at lege
med webassembly, da det lige nu kræver at man benytter Rust i nigtly versionen.
Som jeg var inde på tidligere har pakker fra NPM og Python været et problem på
Windows og Rust er ingen undtagelse. Da Rust er __meget__ nyt kan jeg leve med
det, men jeg er knap så rummelig med NPM og Python.

Rust kommer med et standard bibliotek, men er man vant til C#, finder man
hurtig ud af hvor forkælede vi er med C#, dog kunne man tage en dag hvor
`System` slet ikke var en del af ens `using`. At benytte datoer eller at bygge
en HTTPS klient er temmlig op ad bakke. Smider du SQLite3 i gryden og så har du
en en fest med, at finde ud af hvorfor en given pakke ikke vil bygge. Og igen er
det fordi dem der koder disse tredjeparts biblioteker arbejder på en \*nix
maskine, hvor tingene er langt nemmere, når vi taler om at refere til andre
biblioteker end det er i Windows DLL helvede. Der er dog en flink mand ved navn
Chris Dawes, som har lavet en lille
[guide](https://cmsd2.silvrback.com/rust-msvc) til hvordan man kommer i gang
med OpenSSL, SQLite3 og Rust på Windows. Guiden har bragt mig henover disse
bump forholdsvis nemt og er igen gået i stå ved manglende forståelse for Rust
og dens koncepter. Jeg er heller ikke hjulpet af et fancy IDE, som Visual
Studio og må "nøjes" med Vim. Vim er dog et bevist valg, så ingen medfølelses
her. Det betyder dog at jeg bruger en del tid på at læse god dokumentation og
guides indtil jeg er mere flydende i Rust.

## På dansk?

Ja også er dette min første artikel på dansk og det er ikke den sidste. Ja, jeg
rammer ikke så bredt, men det føles mere naturligt at skrive på modersmålet,
når det igen er til hobby brug. Når en gang jeg kommer til at bruge mit engelsk
noget mere kan det være jeg slår over igen, men for nu er det på dansk.
