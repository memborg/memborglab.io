+++
title = "My first Jekyll post"
description = "A little post about nothing really. Just testing jekyll on local machine"
template = "page.html"
date = 2014-11-19

[taxonomies]
categories = ["writing"]
+++
Cross you fingers!
<!-- more -->

## Aaaand... It worked. 

Even on a Windows PC.

Ruby and Ruby DevKit was installed with
[Chocolatey.org](http://chocolatey.org), a NuGet based Apt-Get system;
everything went smooth.

The rest was just gem commands.

Now it is time to migrate from my selfhosted Wordpress and the pains with
security holes and updates.

Yay! Now running a static blog without any Database backend.

Used this post for reference:
<http://joshualande.com/jekyll-github-pages-poole> and ofcourse
<http://jekyllrb.com/>
