+++

title = "Update on New Insights on Work"
description = "Maybe remote work is pushing teal organizations forward?"

[taxonomies]
categories = ["work"]
tags = ["TIL","teal", "remotework"]

+++

Maybe remote work is pushing teal organizations forward?

<!-- more -->

In my last post: [New Insights on Work](@/2022-07-13-new-insights-on-work.md), I've noted down some discoveries I did lately. And when I heard the recent [podcast from In Depth](https://review.firstround.com/podcast/episode-62) about [Subscript](https://www.subscript.com/) handling remote work in my ears it sounds a lot like a teal organization. They rely on the ability to self-organize as a person and it requires a lot of communication from the CEO. I found it interesting to hear that the CEO who is an engineer picks up tasks and to some programming himself. 
Just a little observation about remote work might be pushing teal organizations forward. 
