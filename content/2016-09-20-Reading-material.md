+++
title = "Reading Material - Blogs, Digest and so on"
description = "I'm reading alot of personal blogs, company blogs, getting inspiration from digests and popular news feeds pertaining my area of expertise. And I would like to share some of the sources I find very interesting. They are in no particular order, preference or grouped by at all."
template = "page.html"
date = 2016-09-20

[taxonomies]
categories = [ "reading" ]
tags = ["blog", "feed", "digest", "news", "reading"]
+++

I'm reading a great deal of personal blogs, company blogs, getting inspiration
from digests and popular news feeds pertaining my area of expertise. And
I would like to share some of the sources I find very interesting. They are in
no particular order, preference or grouped by at all.
<!-- more -->

## [The Loop or The Loop Insight](http://www.loopinsight.com)
This an Apple centric blog with news about the apple factory. It is not a news site per se, more like a blog where the authors picks, some interesting quotes and comments on it with a one liner or maybe two. 

Another way of seeing it, is as a site which collects links to others blog posts because the authors found the link good, funny or just plain dumb. 

I like it for it shortness and how it not very serious.

## [Torrent Freak](https://torrentfreak.com)
This is not a site where you find torrents, but a site about news about piracy, copyright, common sense. Are you curious about how The Pirate Bay stay afloat, mad copyright claims against YouTube or how many DMCA notices Google receives in a year, this is the site to get your fix.

They even keep an updated [list](https://torrentfreak.com/vpn-anonymous-review-160220/) of VPN providers who are serious about anonymity. Torrent Freak have a list of twelve questions they have asked a long list of VPN providers so you don't have to.

## [Mozilla Hacks](https://hacks.mozilla.org)
I presume you're a Fifefox user and if you're a web developer too, you might find Mozilla Hacks a great resource for learning about the latest and greatest about Firefox and its internals.

## [Daring Fireball](http://daringfireball.net/)
Another Apple centric blog, which is written by the (in)famous John Gruber. He has some really good insights in how and why Apple do stuff and often calls out the bullshit some the more mainstream media writes about Apple. 

As it is with any personal blogs sometimes unrelated posts surfaces on this site as well.

## [The Hustle](http://thehustle.co/)
This is a newsletter based news blog. Well, they have a [RSS feed](http://thehustle.co/feed/) too. The format is for mail and it is not your typically tech blog and this is how they put it

> Everyday we comb the internet for the most interesting, most impactful news in business, tech, and entrepreneurship and send them straight to your inbox. Think of it as a daily newspaper that only talks about stuff you care about and is delivered by technology rather than a paper boy making 10¢ an hour. Want to give it a shot? 

## [Alphr](http://www.alphr.com/)
Another tech news sites which is not like all the others. I like it because they give me tech news that I might not find anywhere else or news that normally drowns in more mainstream tech news.

With Alphr's own words
> It’s written for people who embrace new technology and thrive on change, no matter what their job title. It’s a technology site for doers, not dreamers, whether you are one-person start-up or running a major enterprise. And it’s about the whole of your life, because you don’t stop loving technology when you leave the office.

## [Hacker News](https://news.ycombinator.com/)
This is a community driven news site. It is simple and it is small. The site has some small features which boils down to "Ask HN" and "Show HN". Users posts a link and other users choose to comment on it and or up vote comments. 

Sometimes the comment section is a load of bull and other times there are some good pointers.

I'm not there for the social part, but I like the simplicity and how this site also blooms with references to stories I won't get anywhere else.

The site in maintained by the YCombinator Investors Club who funds start ups among others.

## Digests
Finally, I would like to mention some digests I'm following. They are all focused on programming and the world around this e.g. Management, leadership, productivity and so on.
I have ordered these digest by preference, the first one is the best and the last on being the weakest of the four.

1. [Morning Dew](http://www.alvinashcraft.com/)
1. [Javascript Weekly](http://javascriptweekly.com/)
1. [C# Digest](http://csharpdigest.net/)
1. [Morning Brew](http://blog.cwa.me.uk/)
1. [Programming Digest](http://programmingdigest.net/)

## How to keep tabs

One might wonder how do I keep tabs on all of these sites and all those
I haven't recommend?

Well, I have written my own [RSS
consumer](https://gitlab.com/memborg/feed2html.python) in
[Python](https://python.org) 3. I say consumer because reader would be a wrong
word. The program parses all my feeds and generates a simple HTML website I can
load in my browser.

I use [Dropbox](https://dropbox.com) for storage and `https://updog.co`
as my "hosting" provider. An example on how the dumped RSS feeds look like can
be found here `https://memborg.updog.co/20160920/`. There are no logic built
into the site. It is plain and simple HTML with CSS and it works well on small
and big screens.

There are no support for remembering which news I have read on which device and
I'm not intending to add it.

The project does not rely on any third party Python libraries as it is also an
attempt for my to actually learn Python. You might tear your arm of and beat
yourself with it in pure disbelief when you see my approach to date time
parsing or how I have chosen to parse XML.

You might say, this is not how we do Python and your right, this is how this
particular C# programmer does Python.

# UPDATE - 2016-10-30

Swicthed away from Dropbox and Updog. I'm using
[Fastmail](https://fastmail.com) as my mail provider and Fastmail does also
support static HTML hosting. The links to updog noes not work any more, but
another example can be found here
[http://news.superrune.dk/20161030/](http://news.superrune.dk/20161030/).
