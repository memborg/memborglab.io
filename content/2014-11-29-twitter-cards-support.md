+++
title = "Twitter cards support"
description = "Adding Twitter card to blog post and the meta tag description too"
date = 2014-11-29

template = "page.html"
[taxonomies]
categories = ["writing"]
tags = ["twtter", "writing"]

+++

Previously on my selfhosted Wordpress site I've had [Twitter
Cards](https://dev.twitter.com/cards/overview) added to the header of my pages
and I wanted that again.

<!-- more -->

## My cards

If you want to know about [Twitter Cards, you'll find it
here](https://dev.twitter.com/cards/overview). I am focused on _Summary_ and
_Summary with large image_ and nothing else.

In my post config header I have added two properties:

```yaml
description: A short description used for META description and twitter:description
image:
    feature: name-of-image.webp
```

And now for some layout setup on my _\_includes/head.html_

```j2
{%raw%}
{% if page.description %}
<meta name="description" content="{{ page.description }}"/>
{% if page.image.feature %}
<meta name="twitter:card" content="summary_large_image">
{% else %}
<meta name="twitter:card" content="summary">
{% endif%}
<meta name="twitter:site" content="@runememborg">
<meta name="twitter:creator" content="@runememborg">
<meta name="twitter:title" content="{{ page.title }}">
{% if page.description%}
<meta name="twitter:description" content="{{ page.description }}">
{% endif%}
{% if page.image %}
<meta name="twitter:image:src" content="{{ site.url }}/assets/images/{{ page.feature.image }}">
<meta name="twitter:image:width" content="280">
<meta name="twitter:image:height" content="150">
{% endif%}
{% endif%}
{%endraw%}
```

As you can see it is pretty straight forward. Test if there's a description on
the current page; does it have an image too? If so apply it else ignore the
image. I'm using the same image for twitter cards as I'm using as feature
image. It could easily be split into two seperate properties. Maybe that'll
come later on when I need it.

Here are the the final result of the a rendered output taken from [Pocket

-   Autopost to Twitter via IFTTT and Buffer]({% post_url
    2014-03-04-pocket-autopost-to-twitter-via-ifttt-and-buffer %})

```html
<!-- Twitter card -->
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@runememborg" />
<meta name="twitter:creator" content="@runememborg" />
<meta
    name="twitter:title"
    content="Pocket - Autopost to Twitter via IFTTT and Buffer"
/>
<meta
    name="twitter:description"
    content="I'v been working on automating my autoposting of Pocket reads to Twitter with tags and finaly found a solution"
/>
<meta
    name="twitter:image:src"
    content="http://superrune.dk/assets/images/pocket-twitter-ifttt-buffer.webp"
/>
<meta name="twitter:image:width" content="280" />
<meta name="twitter:image:height" content="150" />
```

For now this is my twitter cards setup and META description as a bonus.
