+++
title = "Estimater"
description = "Jeg dykker lidt ned under overfladen og kigger på mit forhold til estimater."
template = "page.html"
date = 2019-03-06

[taxonomies]
categories = [ "work" ]
tags = ["estimation", "planning"]
+++

Jeg dykker lidt ned under overfladen og kigger på mit forhold til estimater.

<!-- more -->

Lige nu er jeg ret grebet af
[#NoEstimates](https://twitter.com/search?q=%23noestimates&src=typd)
bevægelsen, som forsøger at kaster lys over den energi der går i, at estimere
udviklingstiden på et stykke software i stedet for at levere værdi til kunden.
Der diskuteres voldsomt om emnet på [Twitter](https://twitter.com), da det er
et emnet fyldt med religion og idealisme og det fylder så meget i den enkelte
person at tonen mange gange er direkte ubehagelig.

Jeg har selv skrevet et projekt om hvordan man bliver bedre til estimere, men
kan også se, at lige meget hvor meget jeg øver mig bliver det altid et gæt. Du
kan kalde det kvalificeret gæt eller hvad du nu vil, men det er altid et gæt,
når jeg estimerer. Jeg kan forsøge at basere det på noget jeg har bygget før
eller noget jeg måske har estimeret før, men lige lidt hjælper det, når det
kommer til software. Jeg kan ikke estimere hvor lang tid jeg er om at lære
noget og det er i bund og grund det vi gør når vi bygger software. Det er en
læringsproces hver evigt eneste gang vi skal bygge noget til en kunde.

Jeg har i bund og grund ikke noget i mod at estimere en given opgave, da det
i mange tilfælde kan være værdifuldt. Det kan være vi skal give en kunde en
indikation om hvor dyrt noget måske bliver eller vi selv skal vurdere om det
kan betale sig. Og det er sådan set fint nok, når det ikke er muligt at bede
kunden om at investere X kr i, at vi vil prøve noget af for at undersøge om vi
er skaber værdi.

Jeg er lidt ligeglad med om vi estimerer i timer, story points eller hvor mange
bananer jeg kan spise på en dag. I bund grund ender vi med en enhed med en
størrelse på 1 og det er uanset hvilken enhed vi tager. Kigger du f.eks. på
listen over afsluttede opgaver for et givent team og kigger på leadtime vil du
sikkert opdage af mange opgaverne ligger inden den samme tid fra at de bliver
startet til at kunden får dem i hænderne. Det kan være din enhed er 5 dage
eller 2 timer eller 17 bananer. Hvis du skal gætte på noget som helst, er det
måske opgaverne du har løst inden for den sidste måned du skal bruge til, at
forudsige hvor lang tid det næste sæt opgaver tager at løse.

Der hvor kæden hopper af for mig, er når et gæt også bliver til noget jeg har
lovet bliver til virkelighed. Og jeg oven i købet bliver stillet til ansvar for
at gætte forkert, fordi nu har vi lovet nogen at jeg kan gætte rigtigt. Det kan
være mit gæt var for lavt og nu blev det dyre eller vi har sat en deadline ud
fra mit gæt og nu er vi forsinket fordi alle har taget mit gæt for gode vare.
Du synes måske jeg løber fra mit ansvar ved ikke at minde folk om, at det er
gæt eller tuder over et vilkår jeg ikke kan gøre noget ved. Det skulle man tro,
men alle lader sig dupere af et gæt fra en fagmand, som burde vide hvad han
taler om og kunne forudsige hvor lang tid jeg er om at lære noget, men det kan
jeg i virkeligheden ikke. Det kan ikke sammenlignes med en murer der ligger sten
eller en maler der hænger tapet op. Jeg føler heller ikke det er vilkår jeg må leve
med, men noget jeg har indflydelse på.

Vi kunne også gange alle estimater med &#960; eller man kan gøre som i denne
lille tegneserie.

![](/images/commitstrip-esimates.webp)

Lige én til at tænke over

![](/images/large-systems-estimates.webp)

Eller denne

![](/images/project-change-estimate-error.webp)

Hvis vi er så dårlige til at gætte os til hvor lang tid det tager at estimere,
hvorfor så gøre det hele tiden?

Hvorfor er det, at vi ikke bare sætter i gang og levere værdi til vores kunder?
Hvad er det der holder os tilbage i en tilsyneladende gammeldags tankegang?

Jeg har ingen svar på ovenstående, men jeg synes det er mega spændende at tænke
over og observere emnet.

Jeg vil slutte af med et par links:

* [#NoEstimates - Alternative to Estimate-Driven Software Development](http://www.methodsandtools.com/archive/noestimates.php)
* [Story Points Considered Harmful – Or why the future of estimation is really in our past...](http://softwaredevelopmenttoday.com/2012/01/story-points-considered-harmful-or-why-the-future-of-estimation-is-really-in-our-past/)
