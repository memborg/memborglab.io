+++
title = "About taking risks"
description = "A quote about taking risk that resonates with me"
date = 2021-02-19

[taxonomies]
categories = ["quote"]
+++

I stumbled upon this quote which really resonated with me in my current situation and the direction my life might take. 

> A ship is safe in harbor, but that is not what ships are built for

&mdash; author John Shedd


<!-- more -->

To me this is about, it is great where I am now, but I'm capable of so much more and I should take the next step. 
