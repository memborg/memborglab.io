+++
title = "Estimater - Noter"
description = "Agile is not a time driven process"

date = "2019-03-06"

[taxonomies]
categories = ["work"]
tags = ["estimation", "planning"]

[extra]
image = "estimates_4.webp"
+++

Agile is not a time driven process

<!-- more -->

    Velocity is a percentage of = (Ideal time / actual time) * 100.

Jeg er grebet af #NoEstimates bevægelse, hvor vi kigger på værdien af at
estimere i det hele taget. Desuden er der udfordringen med at estimater 60% af
tiden er underestimeret.

![](estimates_4.webp)

![](estimates_5.webp)

## Inspiration

![](estimates_3.webp)

People treat estimates as commitments

![](estimates_2.webp)

Det er det jeg har imod estimater

![](estimates_1.webp)

Estimates er treated as the truth
