+++
title="How to fix LEGO games with no sound with DxWnd"
description="How to fix LEGO games with no sound from fx. Steam with the DxWnd"
date = 2020-06-18

[taxonomies]
categories = [ "gaming" ]
tags = ["steam", "howto", "lego"]

[extra]
image = "images/lego-movie-game.webp"

+++

I am having trouble getting the sound to come out of my living room TV from the
PC I have hooked up to it. And the solution is this little wonder progam called
[DxWnd](https://sourceforge.net/projects/dxwnd/).

<!-- more -->

I love the LEGO games and playing them from my couch alone or with my
significant other. And I'm also moving away from the console platform for
playing games to the PC and have hooked a PC up to my living room TV.

## The problem

This particular LEGO game is The LEGO Movie game, which is not sending any
sound through my HDMI cable and out throuhg my TV speakers. The HDMI could be
connected to a stereo and you still wouldn't get any sound. Why you ask?

Well it seems that the game when running in fullscreen defaults to sending
audio through the main audio port on the mother board. Even if I in Windows 10
disables the speaker device, the game just assumes that no audio device is
available and moves on.

![Deactived sound device](/images/anmaerkning_2020-06-18_105210.webp)

## The solution

I had some experience with this before and the solution were to run the game in
question in window mode instead. For example this works for the great game
Limbo which also have no sound in fullscreen.

But The LEGO Movie games does not come with a built-in option in the settings
menu to run the game in window mode. Well, what now? Another dead end?

After some [duckduckgo'ing](https://duck.com), some one pointed at this little
program called [DxWnd](https://sourceforge.net/projects/dxwnd/). All is does it
taking over the runtime of game and then runs the game in the disered windowed
resolution.

It is pretty basic to get going. Just check the "Run in Window" and push the
"Try..." button and you're of with the gloriuos The LEGO Movie sound and now
able to enjoy the game in its full.

![My settings for The LEGO Movie](/images/anmaerkning_2020-06-18_105348.webp)

If you like, you can [download my configuration](/other/lego-movie-game.dxw) and import into
DxWnd.

Happy gaming!
