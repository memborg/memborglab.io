+++
title="Thoughts about NemID versus Payment gateways"
description = "I've been working with implementing NemID, since 2010 and it has alway baffled me why DanID are doing it the way they do. I will not discuss the bad or good descision when it comes to JAVA or the centralization of the service. "
date = 2013-08-13

[taxonomies]
categories = ["programming"]

[extra]
image = "images/NEMID_RGB_BLACK-side001.webp"

+++
I've been working with implementing NemID, since 2010 and it has alway baffled
me why DanID are doing it the way they do. I will not discuss the bad or good
descision when it comes to JAVA or the centralization of the service, but
merely compare it to how we pay for our goods on a online shop.
<!-- more -->

NemID is designed to be single point of entry to validate a user. This
validation can then be implemented in several service across several sites.
Public sites delivered by the goverment or counties have a single point of
entry where every user must pass through, to gain access to his tax papers,
medical records and so on. But every private company wanting to use NemID has
to do it's own implementation.

## My setup

As a .Net developer I use the .Net source package supplied by DanID because
I have special needs. To be fair, if you do not have the need for changing
their source, there is a precomplied package you just plug into. I need the to
use the source package because I have implemented our NemID solution as a web
service based solution, and that requires, some adjustments to the source.
I have a web service to handle all of our NemID validation. In my web service
I have the following:

1.  A method for showing the JAVA applet
2.  A method for processing wether this is a private user or a company user. If
    it is a private user and he is new, present him with an input field where
    he must enter his CPR number once again. If it's an returning user, try to
    look him up and try to log him on the site. If it s a company user try to
    log him onto the site.
3.  A method for validating the previous input field and finally log him onto
    the website. If the validation fails, send the user back to the input field
    and ask him to try again. Every step, but step 1 is handled by me. I have
    to do the validation of the input field. figure out what to do about if it
    fails and so on. Every now and then, DanID adds new features to the their
    implementation or release a security update. The new features are fx.
    a certificate on a hardware dongle, support for emplyoe signatures and so
    forth. Every time they release a new package I have to adjust my own
    implementation and that is time consuming and in the end our customers
    using our implementation is billed for this update.

## Similar world

Every time a new version is released I cannot help to wonder why they didn't
implement it like most of the payment gateways are. When I want to payment for
my goods I am redirected to a secure page which is controlled by the payment
gateway. Most gateways give you access to design this page to look like the
page they came from. If gateway approves the given payment credentials the
gateway posts transaction information back to an accept page and if the payment
for some reason fails or is cancelled the gateway send the user back to and
fail page. The accept or fail page is handled by the website and the posted
data is handled from there on. As a developer I do not have to worry about
security issues because this is handled by the gateway. If the gateway has any
modifications to the payment flow it's not my concern.

## What I really wanted

Most of the NemID implementation from DanID is already centralized from the
beginning. Fx. the JAVA applet and all the LDAP entries used for validation are
centralized. Why should I invent my own steps after the user is finished with
the applet? Why should I have to worry about security? Why should I have to
figure out the best way to handle company users or hardware dongles? NemID is
almost centralized why not serve the entitre solution that way and the only
thing I have to worry about when the user is validated through NemID is what to
do with the result. I'm not interested in the all the calculations done before
getting the final result, when the user reaches my accept page, if you will,
from NemID I know everything went as planned and the user feels safe.
I personnally believes that this would have been much better if this was
a fully centralized soslution from the beginning. There's a common entry point
for every website who wishes to validate the user thought NemID and a common
exist point for when the use is finished. This would also present the user with
a more unified interface for using  NemID. Today goverment websites are
designed in one way and all the private websites using NemID are presented in
as many ways you possibly can imagine. I do not believe that the user feels
more safe if the log in applet in embedded on the source website or the user is
redirected to a external page for validation. We are used to getting redirected
when paying for goods on a website or using paypal and many people are very
concerned when doing a transaction online.
