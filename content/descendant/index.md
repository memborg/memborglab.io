+++
title = "Descendant"
description = "Apparently I'm a descendant of king slayer"

date = "2020-05-29"

[taxonomies]
categories = ["genealogy"]

[extra]
image = "skarsholm.webp"
+++

Apparently I'm a descendant of king slayer.

<!-- more -->

I might be a descendant of Niels Valdemarsen, son of one of the co-conspirators
behind the murder of king Erik Klipping.

### Links

- <https://www.geni.com/people/Niels-Valdemarsen-Count-of-Halland/6000000007301639475>
- <https://finnholbek.dk/getperson.php?personID=I28252&tree=2>
- <https://da.wikipedia.org/wiki/Mordet_i_Finderup_lade>



