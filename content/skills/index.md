+++
title = "Functional skills"
description = "Creating a resume structured around functional skills sounds like a great idea. If you can add achievements to them too I think you are better positioned."

date = 2025-01-05

[taxonomies]
categories = ["career"]
tags = ["resume"]

[extra]
image = "skills.webp"
+++

Creating a resume structured around functional skills sounds like a great idea.
If you can add achievements to them too I think you are better positioned.

I can get behind this statement:

> Landing your dream job comes down to highlighting your skills

Read for you self: [This New Resume Trick Can Help You Finally Land a Job](https://theeverygirl.com/what-is-a-functional-resume/)

<!-- more -->
