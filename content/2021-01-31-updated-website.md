+++
title = "Updated website"
description = "Added support for dark mode and CI/CD pipeline"
date = 2021-01-31

[taxonomies]
categories = ["website"]
+++

It is time that my personale website moved into this decade with dark mode support and continuous integration and continuous deployment. This enables me to writes post from my mobile phone and every where. This might bring more short posts to the site.


<!-- more -->
