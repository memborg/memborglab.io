+++
title = "Is This Bat Country?"
description = "This could be bat country, I really don't know. I'm heading into, for me untested waters, as project leader working with agile methods."
template = "page.html"
date = 2017-02-08

[taxonomies]
categories = [ "work" ]
tags = ["Project", "leader", "Agile", "New era"]
+++

This could be bat country, I really don't know. I'm heading into, for me
untested waters as a project leader, working with agile methods.
<!-- more -->

The last 6 months or so, ever since I've taken on the role as team lead, my
focus has shifted from being highly interested in writing code to being highly
interested in the people producing the code.

I will probaly continue to code in my spare time, but in my work life, that
part is over and who knows if I ever is getting back to being paid to code.

This brings me to the contents of this blog. Maybe it will shift too and I'll
start writing about people and books I read on people or how I observe or work
with people.

I better stop here this post is getting incoherent, but one last thing; take
the time to read [The Road to
Character](https://www.amazon.com/Road-Character-David-Brooks/dp/0812983416/).
