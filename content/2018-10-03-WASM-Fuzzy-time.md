+++
title = "Rust WASM Fuzzy time"
description = "Endnu et Rust WebAssembly projekt. Denne gang udregning af fuzzy time."
template = "page.html"
date = 2018-10-03

[taxonomies]
categories = [ "programming" ]
+++

Endnu en gang leger jeg lidt med Rust og WeAssembly og emnet er fuzzy-time.
<!-- more -->

Jeg faldt over denne artikel, hvor
[Octobanana](https://octobanana.com/blog/programming-a-fuzzy-time-function) har
implementeret en version i C++ og Javascript. Min tanke, var at dette kunne
være sjovt at implementere i Rust og derefter kompilere det til WebAssembly.

Hvis du ikke helt ved hvad fuzzy time er, så se her:

    32 seconds ago
    4 minutes ago
    1 day ago
    3 weeks ago
    8 months ago
    2 years ago

I stedet for, at skrive et eksakt tidspunkt, skriver man bare hvor lang tid
siden det er sket.

Jeg vil ikke gå i dybden med forskellepå C++, Rust eller JS, men vil du dykke
ned i min implementation i Rust kan du finde [fuzzy_time
her](https://gitlab.com/memborg/fuzzy_time).

En ting er, at skrive noget og få det til at fungere. Noget andet er om der
er vundet noget ved at gøre det. Kan det betale sig, at bruge tiden på
WebAssembly, når nu JS er pænt hurtigt i dag.

Jeg har forsøgt, at lave en lille test ved et genererer en million datoer og så
måle, hvor lang tid JS er om, at udregne en fuzzy time kontra det at sende det
til WebAssembly, som gør det samme.

I FireFox 63.0b11, som jeg køre pånuværende tidspunkt, får jeg dette resultat:

    Generate dates
    1000000 date elements
    get fuzzy time
    JS 0.501 seconds
    get WASM fuzzy time
    WASM 12.281 seconds

Umiddelbart vil jeg sige nej, der er INTET vundet. Måske er min implementation
elendig i Rust, kan der være et kæmpe overhead ved at kalde WebAssembly eller
også er min test for naiv og fejl fyldt.

Lige nu ved jeg det ikke.

Det jeg ved er at jeg synes det er sjovt, at implementere et lille projekt i et
type stærkt sprog, som jeg derfra kan kalde fra min browser uden brug af
plugins, men direkte fra JS.

