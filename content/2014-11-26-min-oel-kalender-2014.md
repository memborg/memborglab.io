+++
title = "Min ølkalender 2014"
description = "Her kommer en lille følgeton om min ølkalender 2014 fra oelhandleren.dk"
template = "page.html"
date = 2014-11-26

[taxonomies]
categories = ["beer"]

[extra]
image = "images/beer2014.webp"

+++

Jeg har gjort mig fortjent til en pakkekalender i år. Sidste gang jeg fik en
var det min kone der besluttede jeg skulle have en. Det kunne jeg ikke byder
hende denne gang, så jeg bestilte den udefra. Det blive fra det lille websted,
kendt som Ølhandleren.

<!-- more -->

Efter at have venter siden tidligt i oktober på at det skulle bliver d. 26.
november 2014 mellem kl. 1800 og 2100 okm dagen endelig. Der blev ringet på
døren og Rune fra ølhandleren stod med 24 indpakkede øl i favnen.

Manden har selv pakket dem flot ind og bringer selv hele herligheden ud så
længe der hvor du bor er brofast og det er hvad jeg kalder service. Så meget
service at jeg føler trang til at fortælle hele verden om min oplevelse.

Hele herligheden kostede mig 490kr inkl. moms, indpakning og levering. det skal
jeg ikke klage over. Jeg vil bare sige tak til min navnebror for denne mulighed
og hvis bare største delen af disse 24 skønheder er gode, så er det nok ikke
sidste gang jeg bestiller øl fra ølhandleren.

At smage på øl fra hele verdenen er gået hen og blevt en hobby for mig. Jeg vil
dog ikke kalde mig connoisseur for så meget nørder jeg heller ikke med det. Jeg
nyder en god øl til maden eller henslængt i sofaen en gang i mellem og nu er
det blevet jul og måske enddag en ekstra god en.

Om 24 dage er vi alle blevet klogere på det, dog forvent ikke de vilde ord
udskejelser.
