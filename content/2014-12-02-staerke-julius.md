+++
title = "Stærke Julius"
description = "Denne gang skal vi stifte bekendtskab med Stærke Julius, en hygge øl."
template = "page.html"
date = 2014-12-02

[taxonomies]
categories = ["beer"]
tags = ["beer", "danish", "5star"]

[extra]
image = "images/br.webp"
+++

Jeg synes det fortsat køre for kalenderen her på anden dagen, dog har vi sta
styrken lidt op. Denne gang skal vi til [Bryggeriet
Refsvindinge](http://www.bryggerietrefsvindinge.dk/). En dansk brygget øl.
<!-- more -->

Jeg har set nogle af deres øl ført i butikkener og må erkende at jeg har bedømt
øllen på den mærkat og nu hvor jeg ikke har noget valg er min bedømmelse gjort
til skamme.

Denne øl synes jeg er en god hygge øl. det er øllen man tager frem, når alle
gaver er pakket ud og børnene lagt i seng og der lige skal ro på efter en god
og lang juleaften. Den er stærk, 8,5% alkohol og smagen af den er også stærk.
Den er ikke bitter ej heller rund, den er lidt spids i det.

![Stærke
Julius](/images/staerke-julius.webp)
