+++
title = "Firefox Configuration"
description = "These are the Firefox configuration in about:config I am using"
date = 2024-03-24


[taxonomies]
categories = ["tools"]
tags = ["Firefox", "browser"]

[extra]
image = "firefox-config.webp"

+++

A fellow blogger [vermanden](https://vermaden.wordpress.com/) shared his
[sensible Firefox
setup](https://vermaden.wordpress.com/2024/03/18/sensible-firefox-setup/) and
some of the setup were in the <about:config> category. There are a few
configurations in there that I found interesting.

<!-- more -->

Especially the configurations around opening everything in new tabs is
something I have been looking for.

From his extensive list I have applied these configurations:

```
ABOUT:CONFIG                                     VALUE      COMMENT
browser.link.open_newwindow.override.external    3          (open external links in a new tab in the last active window )
browser.link.open_newwindow.restriction.         2          (apply the setting to normal windows, but NOT to script windows with features (default))
browser.link.open_newwindow                      3          (ignore open link in new tab/window)
browser.search.openintab                         true       (search bar will open new tab for results)
browser.tabs.animate                             false      (No animation on tabs whatsoever)
browser.tabs.insertRelatedAfterCurrent           false      -
browser.tabs.tabMinWidth                         10         76
browser.tabs.loadBookmarksInTabs                 true       (open links in new tabs)
image.jxl.enabled                                true       (JPEG XL)
pdfjs.defaultZoomValue                           page-width -
```

Head over to [Firefox](https://www.mozilla.org/en-US/firefox/new/) and download the browser now!
