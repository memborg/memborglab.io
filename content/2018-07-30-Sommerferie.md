+++
title = "Sommerferie"
description = "Hvad har bedrevet din ferie med? Jeg har bygget min første iOS app."
template = "page.html"
date = 2018-07-30

[taxonomies]
categories = [ "programming" ]
+++

Jeg håber du har nydt din sommerferie, om du har været i Danmark eller ude
i det store udland.
<!-- more -->

Jeg har opholdt mig i Danmark og nydt den utrolige varme med flere ture i det
blå.

Dog har det ikke og alene været ren afslapning. Der har og været tid til, at
lave lidt hobby kodning og det er også blevet til et par bøger.

## Bøger

Jeg har fået læst to(!) bøger i min ferie.

* **Never Split the Difference: Negotiating As If Your Life Depended On It**
  En bog om hvordan forhandler sig igennem livet skrevet af en tidligere FBI
  agent. Han har nogle gode anekdoter og kommer også med sit bud på en række
  værktøjer, som man kan bruge til, at forhandle en aftale i hus eller få sine
  unger til, at gå seng til tiden.

  ISBN-10: 0062407805
* **Managing Humans: Biting and Humorous Tales of a Software Engineering Manager**
  En bog om hvordan man leder mennesker i en software virksomhed. Den er ganske
  sjov og har nogle fine pointer. Den er både for dig med en teknisk baggrund
  også for dig uden en teknisk baggrund sat til, at lede nørder.

  ISBN-10: 1484221575

## MiniGolf app

Min kone ved godt, at efter jeg er blevet leder ikke koder, så meget mere og
derfor foreslog hun, at jeg kunne starte på en iOS app. Vi kan godt lide et
slag minegolf og her for man altid udleveret en analogt scorecard. Derfor
foreslog hun, at jeg kunne kode en app der kunne erstatte papiret.

Jeg har aldrig kodet en mobil app før. Det har altid været en webapp kørende
i en browser, men nu skulle det altså være native app.

Jeg lånte en MacBook Pro med hjem fra arbejdet og startede med, at se en række
videoer fra iTunes U omkring Swift og iOS11. Efter omkring 15 timers video og
ca. 24 timers kodning var der en app, som kunne oprette et spil og regne
resultatet sammen og finde en vinder.

Det der tog længst tid, var at lære Xcode at kende og hvordan man bygger
grænseflader i Interface Builder. Efter jeg forstod dette var det ingen
hindring at lære Swift 4, da det ligner alle andre morderne sprog. Apple er så
langt med det, at man slet ikke behøver, at forholde sig til hvad Objective-C
er.

Du kan finde [kildekoden her på GitLab](https://gitlab.com/memborg/MiniGolf).

Hvis du har kigget på appen, vil du opleve at den er ret simpel. Der er ikke
noget fancy layout eller design. Alt er det, som Apple levere, eller ikke
leverer. Det hele er et stort navigationview med tableviews og stort set ikke
andet.

Herfra seguer vi mellem layouts og gemmer indtastninger på enheden i CoreData.

Data forlader ikke telefonen, da der ikke var et behov for, at offloade data
til skyen. Derfor vil data gå tabt, hvis man sletter appen eller ikke bruger en
backup, hvis man får en ny enhed. Dog er der indbygget muligheder i CoreData,
der skulle gøre dette nemt, hvis behovet opstod.

**OPDATERING**: Time estimaterne er justeret. De var skudt helt forbi. Når ja,
man er vel software udvikler.

### Et par skærmbilleder

![MiniGolf - Tidligere spil](https://9e1qjq.db.files.1drv.com/y4mCHn4WXbfI70-MoUrpJIzlUNx3P64Ao4OLlQFW-poyUYRWGBR1qsReVhvrWSi-m_BemoFrREwevTV8ixSB3SNV7a2CoohT-CiJOFyWb1otB3AquZ7P8S3N6dAKNsh21iI7mtw0BDhP.webpe-TxV49Q_GVWtv_p28dmCkeqOmASKV33lFnB8ZmkSJa3Zkx_MDqm4zobPJ2Yd4UsQm-qoMgPQ?width=276&height=598&cropmode=none)

![MiniGolf - Spillere](https://901qjq.db.files.1drv.com/y4mxVJsxg7USk93IttN4Z42n9zFKBzaybg2GfkZkcLA1csvzG4Q8uVwYuxhyRRgoE6CK3hihWaPVH81qSBGdcylR4-KYk9k5tomSUXEy0DA_C33lF13YrNe-O1osETtoJVzHgCguXsXObMuuUsuC4IMbksXc60o65HNTTzFhgszpToHfly1cmwe9zwb3CDl-NoNepkRoHUF4X-0-QgRprWTHA?width=276&height=598&cropmode=none)
