+++
title = "Why Uber Engineering Switched From Postgres to MySQL"
description = "Interesting read on why Uber switched from Postgres to MySQL"
template = "page.html"
date = 2016-07-26

[taxonomies]
categories = [ "programming" ]
tags = ["mysql", "postgres", "database"]
+++

Interesting read on why Uber switched from Postgres to MySQL

## TL;DR
Apparently Postgres does not scale really well and data gets corrupted over time.
 
[Read the full blog post here](https://eng.uber.com/mysql-migration/)
<!-- more -->
