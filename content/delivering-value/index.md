+++
title = "Delivering Value"
description = "Start delivering value and stop estimating. The reason being I don’t think it adds any value, and at best it starts a conversation in the team.  #NoEstimates"
date = "2024-03-08"

[taxonomies]
categories = ["agile"]
tags = ["NoEstimates", "podcast", "estimation"]

[extra]
image = "noestimates.webp"
+++

I’m one of those people who thinks estimation is a waste of time and I am
a supporter of the #NoEstimates movement. The reason being I don’t think it
adds any value, and at best it starts a conversation in the team. It does not
add value to the process or the products when we endlessly argue about if it
should be a certain estimate. Estimates does not add value when team members
cop out of the discussion or just agrees to whatever the first estimate is
given. And this applies to estimates given in hours, story points or how many
bananas the team can eat in a sprint.

<!-- more -->

## Deliver!

What matters is that the team can deliver some kind value to the customer in
a given timeframe whether is it a sprint timebox or a deadline the team agreed
on with stakeholders or the customer. We cannot escape that people want to know
when something is done. That will always be present. What we can do is to
minimise how often the questions is asked. One effective way is to start
delivering value to the customer as fast as possible with the desired level of
quality and by being clear about what to expect when said value is delivered.
If the team have proved over and over again that they can deliver value, is it
less important when it is delivered. The reason for that is the customer knows
what to expect when value is delivered and trust that the feature delivered
will be of value. Another way of handling the question is to ask why the
requestor needs the information and how will it be used. If the requestor does
know exactly what the purpose of the estimate the we can dismiss the request
and ask the requestor to come back when he has more information.

## Forecasting

A simple way to figure out when we can deliver something is simply by counting
number PBIs completed in a given time frame. It could be the number of done
PBIs in the last N sprints and find average, minimum and maximum of the number
PBIs completed. This will give you the happy scenario, the worst-case scenario
and something in between. By applying some probability to forecast I will
recommend you look into Monte Carlo forecasting which essentially is doing the
same. Monte Carlo forecasting can of course be applied to hours and story
points, but simply counting PBI is much faster, and the outcome is essentially
the same. It will give you the answer of the question, how much work can we
deliver in the given time box.

## Starting point

I do recognise we all have to start somewhere and being awesome at not
estimating often comes down to how mature, how experienced and how
psychologically safe the team feels. When the team begin to deliver value each
time the deploy a feature to production stakeholders often begins to care less
about how long things take and care more about what is the next thing we should
deliver to our customers.

## Further listening and reading

I like the [Scrum Master Toolbox Podcast](https://scrummastertoolbox.libsyn.com/) and they have a series on #NoEstimates.

This is my recommended listening order

1. [Scrum Master Toolbox Podcast: Agile storytelling from the trenches: From the Archive: BONUS: The art, and science of making prediction with #NoEstimates | Dan Vacanti and Marcus Hammarberg](https://scrummastertoolbox.libsyn.com/from-the-archive-bonus-the-art-and-science-of-making-prediction-with-noestimates-dan-vacanti-and-marcus-hammarberg)

2. [Scrum Master Toolbox Podcast: Agile storytelling from the trenches: From the Archive: A step by step journey to #NoEstimates | Carsten Lützen - NoEstimates Unplugged Week](https://scrummastertoolbox.libsyn.com/from-the-archive-a-step-by-step-journey-to-noestimates-carsten-ltzen-noestimates-unplugged-week)

3. [Scrum Master Toolbox Podcast: Agile storytelling from the trenches: Value Over Velocity, A Product Owner’s Journey to Value-Driven Development, NoEstimates Unplugged Week | Maryse Meinen](https://scrummastertoolbox.libsyn.com/value-over-velocity-a-product-owners-journey-to-value-driven-development-noestimates-unplugged-week-maryse-meinen)

4. [Scrum Master Toolbox Podcast: Agile storytelling from the trenches: Forecast Over Estimation, How to Transform Your Approach to Project Management, NoEstimates Unplugged Week | Luis Garcia](https://scrummastertoolbox.libsyn.com/forecast-over-estimation-how-to-transform-your-approach-to-project-management-noestimates-unplugged-week-luis-garcia)

5. [Scrum Master Toolbox Podcast: Agile storytelling from the trenches: Using NoEstimates to Help Agile Teams Focus on Value, And Create Transparency - NoEstimates Unplugged Week | Lee Beckett](https://scrummastertoolbox.libsyn.com/using-noestimates-to-help-agile-teams-focus-on-value-and-create-transparency-noestimates-unplugged-week-lee-beckett)

If you want to read a book about #NoEstimates, the “NoEstimates - How to
measure project” by Vasco Duarte is a good source.


