+++
title = "Getting a new job"
description = "The job I've been doing for 5 years now is comming to an end"
template = "page.html"
date = 2015-06-15

[taxonomies]
categories = [ "job" ]
tags = ["job", "programming"]
+++

My current job at Tangora Software A/S is soon comming to an end. It been more
than five years since they hired me.
<!-- more -->

I remember applying for a developer spot at Tangora, but they thought I would
be better of in another position. It sounded alright, but it were not very
clear to me what I was going to do at Tangora. There were some front
programming involved on customers solutions. It wasn't until the first at
Tangora I was aware I had to learn C# too.
    
I wasn't familier with C# at the time. I knew PHP and Java and that were it,
but after some time I got the hang of it and now it's my favourite language and
I have them to thank for it.
    
Thanks to the acquired knowledge and C# skills at Tangora I'm now starting
a new adventure at Logicmedia. They are creating their own products for
property owners who are renting their properties. For now I do not precisely
know what I'm going to work on or with whom, but I do know I'm loooking forward
to the change. I'm already pretty excited to be starting at Logicmedia in
August.
    
I also know the last day at Tangora might get emotionally from me. The
collegues are great both when programming as sparing and outside work when we
are socializing. I know I'm going to miss them dearly, but I hope I can find
time to see them outside work.
