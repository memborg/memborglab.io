+++
title = "Slash pages"
description = "Having fun creating all kinds of supporting page to my website that has nothing to with the post, but shuld be informative to you the visitor."

date = 2025-01-05

[taxonomies]
categories = ["website"]
tags = ["improvement"]

[extra]
image = "slashpages.webp"
+++

I am having fun creating all kinds of supporting page to my website that has
nothing to with the posts, but they should be informative to you the visitor.

<!-- more -->

## Pages

The pages are:

* [Hello](/hello) &mdash; How to get in touch with me.
* [About](/about) &mdash; Short intro about me.
* [Colophon](/colophon) &mdash; How is this website produced.
* [Books](/books) &mdash; The books I have read and rated very high.
* [Blogroll](/blogroll) &mdash; All the blogs and podcast I consume.
* [Changelog](/changelog) &mdash; Git hostory with the ten latest commits.
* [Read it later](/later) &mdash; All the links I want to read at some point.

I am probably not done yet and for more inspiration I will keep visiting [slash
pages](https://slashpages.net/).
